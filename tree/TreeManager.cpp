#include "TreeManager.h"
#include <TFile.h>


// Static variables / functions

//____________________________________________________________________________________________________
std::unordered_map<std::string, anytype> TreeManager::s_vars;

//____________________________________________________________________________________________________
void TreeManager::resetCollection() {
    s_vars.clear();
}

//____________________________________________________________________________________________________
void TreeManager::clearVars() {
    for( auto& [key, val] : s_vars ) {
        val.clear();
    }
}

//____________________________________________________________________________________________________
std::unordered_map<std::string, anytype>& TreeManager::collection() {
    return s_vars;
}


// Instance things

//____________________________________________________________________________________________________
TreeManager::TreeManager()
    : m_tree ( nullptr )
{}


//____________________________________________________________________________________________________
TreeManager::~TreeManager() {
    m_tree.release();
}

//____________________________________________________________________________________________________
void TreeManager::initialize(const std::string& treeName,  TFile* file ) {
    if( !file ) {
        throw( std::runtime_error( Form("%s: file ptr is null!", __PRETTY_FUNCTION__ ) ) );
    }
    m_tree.reset( new TTree( treeName.c_str(), treeName.c_str() ) );
    m_tree->SetDirectory( file );
    m_tree->SetAutoFlush( 500. );
}

//____________________________________________________________________________________________________
void TreeManager::book( const std::string& name, anytype& var ) {
    
    if( nullptr == m_tree->GetBranch( name.c_str() ) ) {
        
        var.get().book( name, *m_tree );
        
    }
    
}


//____________________________________________________________________________________________________
void TreeManager::fill() { 
    
    // register variables to the tree
    // a range-based for using C++17 structured bindings!
    for( auto& [key, val] : s_vars ) {
        book( key.c_str(), val );
    }

    m_tree->Fill();
    
}


//____________________________________________________________________________________________________
void TreeManager::fillAndClear() { 
    
    fill();
    clearVars();
    
}
