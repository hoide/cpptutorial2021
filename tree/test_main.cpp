#include "TreeManager.h"

#include <TFile.h>
#include <TRandom3.h>

#include <vector>
#include <memory>
#include <utility>

int main() {

    // first create a new output root file
    auto* of = TFile::Open("test_out.root", "recreate");

    // allocate a new tree manager and hold it as a unique_ptr
    auto tm = std::make_unique<TreeManager>();

    // initialize the tree manager with tree name "test"
    // specify the output TFile in the 2nd argument.
    tm->initialize("test_tree", of);

    // fetch a reference to the variable collection in TreeManager
    // The type of this collection is unordered_map< string, anytype >.
    auto& vars = TreeManager::collection();
    
    // print_type() is a useful custom utility function showing the type of a variable.
    std::cout << "The type of TreeManager::collection() is " << print_type( vars ) << std::endl;

    std::cout << "Loop begin: " << std::endl;

    // Loop over nEntries
    for( auto ientry = 0LL; ientry < 10000LL; ientry++ ) {

        // initialize a vector with a random fixed size
        const auto multDouble = static_cast<size_t>( gRandom->Poisson(10) );
        std::vector<double> varDouble( multDouble );
        
        // Fill elements using Gaussian
        std::generate( varDouble.begin(), varDouble.end(),
                       []() { return gRandom->Gaus( 1, 1 ); } );
        
        // A history-tracking variable used in labmda
        float last { 0.0 };
        
        // Here, a lambda uses a reference capture "&last"
        // The last record of random walk is kept.
        auto randomWalk = [&last]() { 
            last += gRandom->Gaus(0, 1);
            return last;
        };
        
        // initialize a vector with another random fixed size
        const auto multFloat  = static_cast<size_t>( gRandom->Poisson(30) );
        std::vector<float>  varFloat( multFloat );
        
        // Fill elements using random walk lambda
        std::generate( varFloat.begin(), varFloat.end(), randomWalk );
        
        // vector of vector
        std::vector< std::vector<int> > vecVec;
        vecVec.emplace_back( std::vector<int>( { 0, 1, 2 } ) );
        vecVec.emplace_back( std::vector<int>( { 9, 8, 7 } ) );

        // Create variables: does not need to specify the type of vars!
        // Each element of the container vars is "anytype".
        //   - If the variable isn't defined, create an instance.
        //   - Otherwise, copy r.h.s. value to the existing instance.
        //     (Contrary to std::any, the r.h.s. type must be compatible with the existing type)
        //
        // In this manner, the pointer registered by TTree::Branch() is invariant over the process.
        
        vars["varDouble"]   = varDouble;  // vector<double>
        vars["varFloat"]    = varFloat;   // vector<float>
        vars["vecVec"]      = vecVec;     // vector< vector<int> >
        vars["nVarsDouble"] = multDouble; // size_t
        vars["nVarsFloat"]  = multFloat;  // size_t
        vars["bool"]        = true;       // bool
        vars["char"]        = char( 3 );  // char
        vars["short"]       = short( 5 ); // short
        vars["uint"]        = 255u;       // unsigned int
        vars["longlong"]    = ientry;     // long long
        
        // back-filling is supported in TreeManager
        if( ientry >= 123 ) {
            vars["ulong"]       = 255UL;  // unsigned long
            vars["ulonglong"]   = 255ULL; // unsigned long long
            vars["int"]         = -255;   // int
        }
        
        
        // fill the entry to the output tree
        // clear the contents of the variables in all variable collections
        tm->fillAndClear();

    }

    std::cout << "Loop end." << std::endl;
    
    of->cd();
    tm->getTree()->Write();
    of->Close();
    
    // There's no explicit delete!
    
    std::cout << "main() end." << std::endl;
}
