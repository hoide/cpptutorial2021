
#ifndef __COMMONTOOLS_TREEMANAGER__
#define __COMMONTOOLS_TREEMANAGER__

/**
   
   Yet another TTree manager
   
   Author: Hide Oide ( Hideyuki.Oide@cern.ch )
   
   Created: 2018-MAR-11
   Update:  2021-JUL-27

 */

#include "anytype.h"

#include <TTree.h>

#include <string>
#include <unordered_map>

class TFile;

// TTree manager
class TreeManager {
 protected:
  
  std::unique_ptr<TTree> m_tree;
  
  static std::unordered_map<std::string, anytype > s_vars;
  
 public:
  
  static void resetCollection();
  static void clearVars();
  static std::unordered_map<std::string, anytype>& collection();
  
  TreeManager();
  virtual ~TreeManager();
  
  void initialize(const std::string&,  TFile* );

  template<class T>
  void book( const char* name, T* var );
  
  virtual void book( const std::string&, anytype& var );
  void fill();
  void fillAndClear();

    inline TTree* getTree() { return m_tree.get(); }
  
};


template<class T>
void TreeManager::book( const char* name, T* var ) {
  if( nullptr == m_tree->GetBranch( name ) ) {
    m_tree->Branch( name, var );
  } else {
    auto* branch = m_tree->GetBranch( name );
    m_tree->SetBranchAddress( name, &var, &branch );
  }
}

#endif /* __COMMONTOOLS_TREEMANAGER__ */
