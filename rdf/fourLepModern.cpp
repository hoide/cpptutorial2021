#include "Lepton.h"
#include "zip.h"

#include "TFile.h"
#include "TSystem.h"

#include <ROOT/RDataFrame.hxx>

#include <vector>
#include <map>
#include <iostream>
#include <thread>

using Ref_Lepton = std::reference_wrapper< Lepton >;


auto selectEvents = []( ROOT::RDataFrame& df, const std::string_view dataname, const double weight, const double intLumi ) {
    
    auto filtered = df
            
    //
    // Step-0: filter events by trigger
    //
          
    .Filter("trigE || trigM")
            
    //
    // Step-1: define lepton objects from ntuple variables
    //
          
    .Define( "Leptons",
             []( std::vector<unsigned int>& type,
                 std::vector<float>& pt, 
                 std::vector<float>& eta,
                 std::vector<float>& phi,
                 std::vector<float>& E,
                 std::vector<int>& charge,
                 std::vector<float>& d0,
                 std::vector<float>& z0,
                 std::vector<float>& d0Sigma,
                 std::vector<float>& ptcone30,
                 std::vector<float>& etcone20 )
    {

        // We are going to create a collection of lepton objects
        // variables are: type, 4-vector, charge, impact parameters and isolation
        std::vector<Lepton> leptons;
                      
        for( size_t i=0; i<pt.size(); i++) {
            leptons.emplace_back( Lepton( type.at(i),
                                          pt.at(i), eta.at(i), phi.at(i), E.at(i),
                                          charge.at(i),
                                          d0.at(i), z0.at(i), d0Sigma.at(i),
                                          ptcone30.at(i), etcone20.at(i) ) );
        }

        // Sort leptons in descending order ot pT
        std::sort( leptons.begin(), leptons.end(), []( auto& l1, auto& l2 ) { return l1.pt() > l2.pt(); } );
        
        return leptons;   },
             
    { "lep_type", "lep_pt", "lep_eta", "lep_phi", "lep_E", "lep_charge",
      "lep_trackd0pvunbiased", "lep_z0",
      "lep_tracksigd0pvunbiased", "lep_ptcone30", "lep_etcone20" } )
            
            
    //
    // Step-2: select only good leptons
    // In the lepton object, Lepton::isGood() is equipped (see above)
    // Using this function, extract only good leptons and store them to another variable "GoodLeptons".
    //
            
    .Define( "GoodLeptons",
             []( std::vector<Lepton>& leptons )
    {
        std::vector< Ref_Lepton > goodLeptons;
        std::copy_if( leptons.begin(), leptons.end(), std::back_inserter( goodLeptons ),
                      []( auto& lepton ) { return lepton.isGood(); } );
                         
        return goodLeptons;
    }, { "Leptons" } )
          
    //
    // Step-3: select exact 4-lepton events only
    //
            
    .Filter( []( std::vector< Ref_Lepton >& leptons )
    {
        return leptons.size() == 4;
    }, { "GoodLeptons" } )
            
    //
    // Step-4: select only events with total sum-charges of 4-leptons is neutral
    //
    
    .Filter( []( std::vector< Ref_Lepton >& leptons )
    {
        auto sumCharges = std::accumulate( leptons.begin(), leptons.end(), 0,
                                           []( auto n, Lepton& lepton ) { return n + lepton.charge(); } );
        return sumCharges == 0;
    }, { "GoodLeptons" } )
          
    //
    // Step-5: select only events with 4e, 4mu or ee+mumu
    // lepton type is abs(pdgid)
    // pdgid 11: electron
    // pdgid 13: muon
    // 
    // 4e      --> sum = 44
    // 4mu     --> sum = 52
    // ee+mumu --> sum = 48
    //
            
    .Filter( [](  std::vector< Ref_Lepton >& leptons )
    {
        enum { FourElectrons = 44, FourMuons = 52, TwoEleTwoMu = 48 };

        auto sumTypes = std::accumulate( leptons.begin(), leptons.end(), 0,
                                         []( auto n, Lepton& lepton ) { return n + lepton.type(); } );
        return sumTypes == FourElectrons || sumTypes == FourMuons || sumTypes == TwoEleTwoMu;
    }, { "GoodLeptons" } )
          
    //
    // Step-6: pT selection
    //   Leading lepton: > 25 GeV
    //   2nd leading:    > 15 GeV
    //   3rd leading:    > 10 GeV
    //
            
    .Filter( []( std::vector< Ref_Lepton >& leptons )
    {
        const std::vector<float> thresholds { 25.0, 15.0, 10.0, 0.0 };
        
        assert( thresholds.size() == leptons.size() );
                             
        std::vector<char> flags;
                             
        std::transform( leptons.begin(), leptons.end(), thresholds.begin(), 
                        std::back_inserter( flags ),
                        []( Lepton& lepton, auto& thr ) { return lepton.pt() > thr; } );
        
        // return true if all pT are above the threshold of each
        auto flag = std::accumulate( flags.begin(), flags.end(), true,
                                     []( bool f1, auto& f ) { return f1 && f; } );
                             
        return flag;
    }, { "GoodLeptons" } )
                
            
    //
    // Step-7: compose 4-lepton mass
    //
            
    .Define( "m4L", []( std::vector< Ref_Lepton >& leptons )
    {
        
        auto sumP4 = std::accumulate( leptons.begin(), leptons.end(), TLorentzVector(),
                                      []( auto& psum, Lepton& l ) { return (psum + l.p4()); } );
        
        return sumP4.M();
        
    }, { "GoodLeptons" } )
          
    ;
    
    
    if( dataname == "Data" ) {
        
        filtered = filtered.Define("weight", "1.0");
        
    } else {
        
        filtered = filtered
        .Define("weight", Form("scaleFactor_ELE * scaleFactor_MUON * scaleFactor_LepTRIGGER * scaleFactor_PILEUP * mcWeight / %f * %f", weight, intLumi) );
        
    }
    
    return filtered;
};


void FourLep() {
    
    ROOT::EnableImplicitMT();
    
    const double intLumi { 10064.0 }; // [pb-1]

    const std::string path { "4lep" };

    std::unordered_map<std::string, std::vector<std::string> > files;
    files["Data"]  = { "Data/data_A", "Data/data_B", "Data/data_C", "Data/data_D" };
    files["Higgs"] = { "MC/mc_345060.ggH125_ZZ4lep", "MC/mc_344235.VBFH125_ZZ4lep" };
    files["ZZ"]    = { "MC/mc_363490.llll" };
    files["others"]= { "MC/mc_361106.Zee", "MC/mc_361107.Zmumu" };
    
    std::unordered_map<std::string, std::vector<float> > weightMap;
    weightMap["Data"]  = { 1.0, 1.0, 1.0, 1.0 };
    weightMap["Higgs"] = { 27881776.6536 / 0.0060239, 3680490.83243 / 0.0004633012 };
    weightMap["ZZ"]    = { 7538705.8077 / 1.2578 };
    weightMap["others"]= { 150277594200 / 1950.5295, 147334691090 / 1950.6321 };
    
    
    std::unordered_map<std::string, std::vector<ROOT::RDataFrame> > dataset;
    
    
    // Open input files
    // In C++17 the structured binding feature
    // enables to expand pair's [key, value] in range-based for loop.
    for( auto& [dataname, samples] : files ) {
      
        std::vector<ROOT::RDataFrame> frames;

        std::transform( samples.begin(), samples.end(), std::back_inserter( frames ),
                        [&]( auto& sample ) {

                            std::vector<std::string> tokens { path, sample };
                            
                            auto name = std::accumulate( std::next(tokens.begin()), tokens.end(), tokens.at(0),
                                                         []( std::string out, std::string token )  { return out + "/" + token; } );
                            
                            name += ".4lep.root";
                            std::cout << name << std::endl;
                            
                            return ROOT::RDataFrame( "mini", name ); } );

        dataset[ dataname ] = frames;
    }
    
    
    // Refresh output histogram output file
    TFile::Open("out_hist.root", "recreate")->Close();
    
    
    // Loop over all dataset
    for( auto& [dataname, samples] : dataset ) {
        
        gSystem->mkdir( "snapshot" );
        gSystem->mkdir( Form("snapshot/%s", dataname.c_str() ) );
        
    
        std::vector< ROOT::RDF::RResultPtr<TH1D> > outHists;
        
        std::cout << "\n--------------------" << std::endl;
        std::cout << dataname << std::endl;
        std::cout << "--------------------" << std::endl;
        
        auto& weights = weightMap.at( dataname );
        
        
        
        auto process = [&]( auto sample, auto weight, auto subname ) {
                            
            auto filtered = selectEvents( sample, dataname, weight, intLumi );
            
            std::cout << subname << std::endl;
            
            // Save histogram (lazy evaluation)
            //   - 1st arg: parameter list to construct the 1D histogram { name, title, nbins, min, max }
            //   - 2nd arg: variable to fill
            //   - 3rd arg: fill weight
            auto hist = filtered.Histo1D( { ("h_m4L_" + dataname).c_str(), ";m_{4L} [GeV];Events", 110, 80, 300 }, "m4L", "weight" );
            
            outHists.emplace_back( hist );
            
            subname.replace( subname.find("/"), 1, "_" );
            
            
            // Save events snapshot
            //   - 1st arg: output tree name
            //   - 2nd arg: output file name
            //   - 3rd arg: list of variables to save
            
            filtered.Snapshot( "out_mini", Form("snapshot/%s/%s.root", dataname.c_str(), subname.c_str() ),
                               { "runNumber", "eventNumber", "m4L", "weight" } );
            
        };
            
        
        
        //-----------------------------------------------------------------------------------------
        // Multithread processing
        // Use std::thread to handle a thread
        std::vector<std::thread> threads;
        
        // Here, a helper function zip in zip.h is used.
        // It output a vector of tuples of input containers
        // In C++17, the output tuple can be handled by so-called "structured binding"
        
        for( auto [sample, weight, subname] : zip( samples, weights, files[dataname] ) ) {
                
            threads.emplace_back( std::thread( process, sample, weight, subname ) );
                
        }
            
        for( auto& t : threads ) t.join();
        //-----------------------------------------------------------------------------------------
        
        
        
        // add-up sub-datasets
        auto* hout = std::accumulate( std::next(outHists.begin()), outHists.end(),
                                      outHists.at(0).GetPtr(),
                                      []( auto* hout, auto& h ) { hout->Add( h.GetPtr() ); return hout; } );
    
        auto* ofile = TFile::Open("out_hist.root", "update");
        hout->Write();
        ofile->Close();
        
    }
    
}


int main( int /*argc*/, char** /*argv*/ ) {
    
    FourLep();
    
    return 0;
}
