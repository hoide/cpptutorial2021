#ifndef Lepton_H
#define Lepton_H

#include "TLorentzVector.h"

class Lepton {
private:
    unsigned int m_type;
    TLorentzVector m_p4;
    int m_charge;
    float m_d0;
    float m_z0;
    float m_d0Sigma;
    float m_ptcone30;
    float m_etcone20;
    
public:
    Lepton( unsigned int type,
            float pt, float eta, float phi, float E,
            int charge,
            float d0, float z0, float d0Sigma,
            float ptcone30, float etcone20 )
        : m_type( type )
        , m_p4()
        , m_charge( charge )
        , m_d0( d0 )
        , m_z0( z0 )
        , m_d0Sigma( d0Sigma )
        , m_ptcone30( ptcone30 / 1.e3 ) // record in [GeV]
        , m_etcone20( etcone20 / 1.e3 ) // record in [GeV]
    {
        m_p4.SetPtEtaPhiE( pt/1.e3, eta, phi, E/1.e3 ); // record in [GeV]
    }
    
    // Various accessors
    int type()           const { return m_type; }
    TLorentzVector p4()  const { return m_p4; }
    float pt()           const { return m_p4.Pt(); }
    float eta()          const { return m_p4.Eta(); }
    float phi()          const { return m_p4.Phi(); }
    int charge()         const { return m_charge; }
    float d0()           const { return m_d0; }
    float z0()           const { return m_z0; }
    float d0Sigma()      const { return m_d0Sigma; }
    float d0Signif()     const { return std::abs( m_d0 / m_d0Sigma ); }
    float ptcone30()     const { return m_ptcone30; }
    float etcone20()     const { return m_etcone20; }
    float ptIsoFrac()    const { return m_ptcone30 / m_p4.Pt(); }
    float etIsoFrac()    const { return m_etcone20 / m_p4.Pt(); }
    bool isElectron()    const { return m_type == 11; }
    bool isMuon()        const { return m_type == 13; }
    
    bool isGood() const {
        
        if( abs( eta() ) > 2.5 )                       return false;
        if( pt() < 5. )                                return false;
        if( ptIsoFrac() > 0.3 )                        return false;
        if( etIsoFrac() > 0.3 )                        return false;
        
        if( d0Signif() > 5.0 ||
            std::abs(z0() * sin(p4().Theta())) > 0.5 ) return false;
        
        if( isElectron() ) {
            if( pt() < 7.0 || std::abs( eta() ) > 2.47 ) return false;
        }
        
        return true;
    }
};

// Sort lepton objects by descending order of pT
bool operator<( const Lepton& l1, const Lepton& l2 ) {
    return l1.pt() > l2.pt();
}


#endif /* Lepton_H */
