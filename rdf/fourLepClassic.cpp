#include "Lepton.h"

#include "TLorentzVector.h"
#include "TChain.h"
#include "TH1F.h"
#include "TFile.h"
#include "TSystem.h"

#include <iostream>

void process( TChain* chain, std::string dataname, std::string subname, float weight, float intLumi, TH1* hm4L ) {
    
    int runNumber;
    int eventNumber;
    
    bool trigE;
    bool trigM;
    
    std::vector<unsigned int> * lep_type     = 0;
    std::vector<float>        * lep_pt       = 0; 
    std::vector<float>        * lep_eta      = 0;
    std::vector<float>        * lep_phi      = 0;
    std::vector<float>        * lep_E        = 0;
    std::vector<int>          * lep_charge   = 0;
    std::vector<float>        * lep_d0       = 0;
    std::vector<float>        * lep_z0       = 0;
    std::vector<float>        * lep_d0Sigma  = 0;
    std::vector<float>        * lep_ptcone30 = 0;
    std::vector<float>        * lep_etcone20 = 0;
    
    chain->SetBranchAddress( "runNumber",                &runNumber);
    chain->SetBranchAddress( "eventNumber",              &eventNumber);
    
    chain->SetBranchAddress( "trigE",                    &trigE);
    chain->SetBranchAddress( "trigM",                    &trigM);
    
    chain->SetBranchAddress( "lep_type",                 &lep_type);
    chain->SetBranchAddress( "lep_pt",                   &lep_pt);
    chain->SetBranchAddress( "lep_eta",                  &lep_eta);
    chain->SetBranchAddress( "lep_phi",                  &lep_phi);
    chain->SetBranchAddress( "lep_E",                    &lep_E);
    chain->SetBranchAddress( "lep_charge",               &lep_charge);
    chain->SetBranchAddress( "lep_trackd0pvunbiased",    &lep_d0);
    chain->SetBranchAddress( "lep_tracksigd0pvunbiased", &lep_d0Sigma);
    chain->SetBranchAddress( "lep_z0",                   &lep_z0);
    chain->SetBranchAddress( "lep_ptcone30",             &lep_ptcone30);
    chain->SetBranchAddress( "lep_etcone20",             &lep_etcone20);
    
    float scaleFactor_ELE;
    float scaleFactor_MUON;
    float scaleFactor_LepTRIGGER;
    float scaleFactor_PILEUP;
    float mcWeight;
    
    chain->SetBranchAddress( "scaleFactor_ELE",        &scaleFactor_ELE );
    chain->SetBranchAddress( "scaleFactor_MUON",       &scaleFactor_MUON );
    chain->SetBranchAddress( "scaleFactor_LepTRIGGER", &scaleFactor_LepTRIGGER );
    chain->SetBranchAddress( "scaleFactor_PILEUP",     &scaleFactor_PILEUP );
    chain->SetBranchAddress( "mcWeight",     &mcWeight );
    
    subname.replace( subname.find("/"), 1, "_" );
    TFile* ofile = TFile::Open( Form("snapshot/%s/%s.root", dataname.c_str(), subname.c_str()), "recreate" );
    TTree* otree = new TTree("out_mini", "out_mini");
    
    double v_m4L     = 0.0;
    double v_weight  = 0.0;
    
    otree->Branch( "runNumber", &runNumber );
    otree->Branch( "eventNumber", &eventNumber );
    otree->Branch( "m4L", &v_m4L );
    otree->Branch( "weight", &v_weight );
    
    
    for( long long ievent = 0; ievent < chain->GetEntries(); ievent++ ) {
        
        chain->GetEntry( ievent );
        
        //
        // Step-0: filter events by trigger
        //
          
        if( !(trigE || trigM) ) continue;
        
        //
        // Step-1: define lepton objects from ntuple variables
        //
          
        std::vector<Lepton*> leptons;
        
        for( size_t il = 0; il < lep_type->size(); il++ ) {
            
            leptons.push_back( new Lepton( lep_type->at(il),
                                           lep_pt->at(il), lep_eta->at(il), lep_phi->at(il), lep_E->at(il),
                                           lep_charge->at(il),
                                           lep_d0->at(il), lep_z0->at(il), lep_d0Sigma->at(il),
                                           lep_ptcone30->at(il), lep_etcone20->at(il) ) );
            
        }
        
        // Sort leptons in descending order ot pT
        std::sort( leptons.begin(), leptons.end() );
        
        
        //
        // Step-2: select only good leptons
        // In the lepton object, Lepton::isGood() is equipped (see above)
        // Using this function, extract only good leptons and store them to another variable "GoodLeptons".
        //
            
        std::vector<Lepton*> goodLeptons;
        
        for( std::vector<Lepton*>::iterator lep_itr = leptons.begin();
             lep_itr != leptons.end();
             ++lep_itr ) {
            
            Lepton* lepton = *lep_itr;
            if( lepton->isGood() ) {
                goodLeptons.push_back( lepton );
            }
        }
        
        //
        // Step-3: select exact 4-lepton events only
        //
        
        if( goodLeptons.size() != 4 ) continue;
        
        //
        // Step-4: select only events with total sum-charges of 4-leptons is neutral
        //
        
        int sumCharges = 0;

        for( std::vector<Lepton*>::iterator lep_itr = goodLeptons.begin();
             lep_itr != goodLeptons.end();
             ++lep_itr ) {
            
            Lepton* lepton = *lep_itr;
            sumCharges += lepton->charge();
        }
        
        if( sumCharges != 0 ) continue;
        
            
        //
        // Step-5: select only events with 4e, 4mu or ee+mumu
        // lepton type is abs(pdgid)
        // pdgid 11: electron
        // pdgid 13: muon
        // 
        // 4e      --> sum = 44
        // 4mu     --> sum = 52
        // ee+mumu --> sum = 48
        //
            
        int fourLeptonTypes = 0;

        for( std::vector<Lepton*>::iterator lep_itr = goodLeptons.begin();
             lep_itr != goodLeptons.end();
             ++lep_itr ) {
            
            Lepton* lepton = *lep_itr;
            fourLeptonTypes += lepton->type();
        }
        
        if( !( fourLeptonTypes == 44 || fourLeptonTypes == 48 || fourLeptonTypes == 52 ) ) continue;
        
        //
        // Step-6: pT selection
        //   Leading lepton: > 25 GeV
        //   2nd leading:    > 15 GeV
        //   3rd leading:    > 10 GeV
        //
        
        if( !( goodLeptons.at(0)->pt() > 25.0 ) ) continue;
        if( !( goodLeptons.at(1)->pt() > 15.0 ) ) continue;
        if( !( goodLeptons.at(2)->pt() > 10.0 ) ) continue;
        
        //
        // Step-7: compose 4-lepton mass
        //
            
        TLorentzVector sumP4 = goodLeptons.at(0)->p4() + goodLeptons.at(1)->p4() + goodLeptons.at(2)->p4() + goodLeptons.at(3)->p4();
        
        double m4L = sumP4.M();
        
        double eventWeight = 1.0;
        
        if( dataname != "Data" ) {
            eventWeight = scaleFactor_ELE * scaleFactor_MUON * scaleFactor_LepTRIGGER * scaleFactor_PILEUP * mcWeight / weight * intLumi;
        }
        
        hm4L->Fill( m4L, eventWeight );
	
	
	v_m4L    = m4L;
	v_weight = eventWeight;
	
	ofile->cd();
	otree->Fill();
        
        for( std::vector<Lepton*>::iterator lep_itr = leptons.begin();
             lep_itr != leptons.end();
             ++lep_itr ) {
            
            delete *lep_itr;
        }
        
    } // end of event loop
    
    
    otree->Write();
    ofile->Close();
    
    return;
}


void FourLepClassic() {
    
    const double intLumi { 10064.0 }; // [pb-1]

    const std::string path { "4lep" };

    std::map<std::string, std::vector<std::string> > files;
    files["Data"].push_back("Data/data_A");
    files["Data"].push_back("Data/data_B");
    files["Data"].push_back("Data/data_C");
    files["Data"].push_back("Data/data_D");
    
    files["Higgs"].push_back("MC/mc_345060.ggH125_ZZ4lep");
    files["Higgs"].push_back("MC/mc_344235.VBFH125_ZZ4lep");
    
    files["ZZ"].push_back("MC/mc_363490.llll");
    
    files["others"].push_back("MC/mc_361106.Zee");
    files["others"].push_back("MC/mc_361107.Zmumu");
    
    std::map<std::string, std::vector<float> > weightMap;
    weightMap["Data"].push_back( 1.0 );
    weightMap["Data"].push_back( 1.0 );
    weightMap["Data"].push_back( 1.0 );
    weightMap["Data"].push_back( 1.0 );
    
    weightMap["Higgs"].push_back( 27881776.6536 / 0.0060239 );
    weightMap["Higgs"].push_back( 3680490.83243 / 0.0004633012 );
    
    weightMap["ZZ"].push_back( 7538705.8077 / 1.2578 );
    
    weightMap["others"].push_back( 150277594200 / 1950.5295 );
    weightMap["others"].push_back( 147334691090 / 1950.6321 );
    
    
    TFile::Open("out_hist.root", "recreate")->Close();;
    
    // Open input files
    for( std::map< std::string, std::vector<std::string> >::iterator files_itr = files.begin();
         files_itr != files.end();
         ++files_itr ) {
        
        const std::string& dataname = files_itr->first;
        const std::vector<std::string>& samples = files_itr->second;
        
        const std::vector<float>& weights = weightMap[ dataname ];
        std::vector<float>::const_iterator weight_itr = weights.begin();
        
        TH1F* hm4L = new TH1F( ("h_m4L_" + dataname).c_str(), ";m_{4L} [GeV];Events", 110, 80, 300 );
        
        gSystem->mkdir( "snapshot" );
        gSystem->mkdir( Form("snapshot/%s", dataname.c_str() ) );
	
        // loop over subsamples
        for( std::vector<std::string>::const_iterator sample_itr = samples.begin();
             sample_itr != samples.end();
             ++sample_itr ) {
            
            // filename of the subsample
            std::string filename = path + "/" + *(sample_itr) + ".4lep.root";
            std::cout << filename << std::endl;
            
            TChain* chain = new TChain("mini", "mini");
            chain->AddFile( filename.c_str() );
            
            // main loop
            process( chain, dataname, *(sample_itr), *(weight_itr), intLumi, hm4L );
            
            delete chain;
            
            ++weight_itr;
        }
        
        auto* ofile = TFile::Open("out_hist.root", "update");
        ofile->cd();
        hm4L->Write();
        ofile->Close();
    }
    
    
    return;
}


int main( int /*argc*/, char** /*argv*/ ) {
    
    FourLepClassic();
    
    return 0;
}
