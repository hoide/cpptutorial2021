#!/bin/sh

if [ ! -f 4lep.zip ]; then
    wget https://atlas-opendata.web.cern.ch/atlas-opendata/samples/2020/4lep.zip
else:
    echo "file already downloaded"
fi

unzip 4lep.zip
