# CppTutorial2021

Example codes introducing new C++ features for C++ introductory lecture in KEK summer 2021.

There are a couple of examples

* In the directory `tree`, an example code of generating a ROOT tree using a helper class ``TreeManager'' is provided.
  In this example, the user who creates his/her own tree does not need boilerplate lines:  
```
  tree->Branch("variable", &variable);
```  
  anymore. All variables are temporarily stored in a container in TreeManager making use of ``anytype'' class.


* In the directory `rdf`, two example codes of reading ATLAS open data of higgs to 4-leptons.
  The first example `fourLepClassic` uses grammars of C++98/03.
  The second example `fourLepModern` uses the modern C++17 techniques together with recent ROOT `RDataFrame` framework.


Building environment is g++ (GCC) 10.1.0 and ROOT Version: 6.24/00 (compiled with -std=c++17).
If the envrironment used CVMFS, this environment can be setup by the following script:

```
$ source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc10-opt/setup.sh
```

In each directory a simple Makefile is provided to build the code.

